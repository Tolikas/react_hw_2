import './App.css';
import {
  Route,
  Link,
  Routes,
  BrowserRouter
} from "react-router-dom";

import TemplatePage from './TemplatePage';
import CounterPage from './CounterPage';
import AppBar from '@mui/material/AppBar';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import page404 from "./page404.png";
import pageLogin from "./pageLogin.png";
import pageRegister from "./pageRegister.png";
import pageHome from "./pageHome.png";

function App() {
  return (
    <BrowserRouter>
      <AppBar position="static">
        <Tabs 
          centered
          textColor="inherit"
        >
          <Tab label="Login" component={Link} to="/Login"/>
          <Tab label="Register" component={Link} to="/Register"/>
          <Tab label="Home" component={Link} to="/Home"/>
          <Tab label="404" component={Link} to="/404"/>
        </Tabs>
      </AppBar>
      <Routes>
        <Route path="Login" element={<TemplatePage background = {pageLogin} />} />
        <Route path="Register" element={<TemplatePage background = {pageRegister} />} />
        <Route path="Home" element={<CounterPage background = {pageHome} />} />
        <Route path="404" element={<TemplatePage background = {page404} />} />
        <Route path="*" element={<TemplatePage background = {pageLogin} />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;