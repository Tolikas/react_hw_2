const TemplatePage = (props) => {

    const css = {
        // display: 'flex',
        // alignItems: 'center',
        // justifyContent: 'center',
        // height: '100vh',
        backgroundImage: `url(${props.background})`,
        backgroundPosition: 'center',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        width: '100vw',
        height: '100vh'        
    };

    // const css2 = {
    //     backgroundImage: `url(${props.background})`,
    //     backgroundPosition: 'center',
    //     backgroundSize: 'cover',
    //     backgroundRepeat: 'no-repeat',
    //     width: '100vw',
    //     height: '100vh'
    // };

    //return <div style={css}>Это страница "{props.pageName}", количество посещений = {props.numberOfVisits}</div>
    // return <div style={css}>Это страница "{props.pageName}"</div>
    return <div style={css}></div>
};

export default TemplatePage;