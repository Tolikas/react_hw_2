import React from "react";
import Counter from "./components/Counter";

import {Provider} from "react-redux";
import store, {Persistor} from "./store";
import {PersistGate} from "redux-persist/integration/react";

const CounterPage = (props) => {

    const styles = {
        fontFamily: "sans-serif",
        textAlign: "center",
        backgroundImage: `url(${props.background})`,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundPosition: 'center',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        width: '100vw',
        height: '100vh'       
    };

    return (
        <Provider store={store}>
            <PersistGate loading={null} persistor={Persistor}>
                <div style={styles}>
                    <div>
                        <Counter />
                    </div>
                </div>
            </PersistGate>
        </Provider>
    );
};

export default CounterPage;